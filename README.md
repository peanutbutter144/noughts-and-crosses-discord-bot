## Noughts and Crosses (Discord bot)

This is how the board will look:

```
+---+---+---+ 
| O | O |   | 
+---+---+---+ 
| X | O | O | 
+---+---+---+ 
| X |   | X | 
+---+---+---+ 
```

Note: this is a practice. It may be buggy, and noughts and crosses is probably
already incorporated into another Discord bot. But I wanted to try my hand at
making one, so here it is.
